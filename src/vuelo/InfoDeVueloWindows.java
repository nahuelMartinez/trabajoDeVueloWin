package vuelo;

import org.uqbar.arena.windows.Window;

import java.awt.Color;


import org.uqbar.arena.layout.ColumnLayout;
import org.uqbar.arena.widgets.Button;
import org.uqbar.arena.widgets.GroupPanel;
import org.uqbar.arena.widgets.Label;
import org.uqbar.arena.widgets.Panel;
import org.uqbar.arena.widgets.tables.Column;
import org.uqbar.arena.widgets.tables.Table;


import Aerolinea.Aerolines.Pasaje;
import Vuelos.Vuelo;

public class InfoDeVueloWindows extends Window <Vuelo>{
	private static final long serialVersionUID = 1L;

	
	
	private VueloWindow vantanaAnterior;
	private VuelosController controlerDeVueloWindow;
	



	public InfoDeVueloWindows  ( VueloWindow owner , Vuelo model,VuelosController controler){
		super(owner ,model);
		this.vantanaAnterior = owner;
		this.controlerDeVueloWindow=controler;
	}
	
	

	@Override
	public void createContents(Panel arg0) {
	
	GroupPanel cajaRoja = new GroupPanel(arg0);
	cajaRoja.setTitle("VueloSeleccionado");

	
	
	 Panel cajaAzul = new Panel(arg0);
		cajaAzul.setLayout(new ColumnLayout(4));
		
		
		new Label(cajaAzul)
		.setText("Fecha:")
		.setFontSize(15);
		new Label (cajaAzul)
		.bindValueToProperty("fechaDeVuelo");
		
		new Label(cajaAzul)
		.setText("Asintos libres:")
		.setFontSize(9);
		new Label (cajaAzul)
		.bindValueToProperty("cantidadDeAsientosLibres");
		
		new Label(cajaAzul)
		.setText("Destino:")
		.setFontSize(9);
		new Label (cajaAzul)
		.bindValueToProperty("destino");
		
		new Label(cajaAzul)
		.setText("Precio actual:")
		.setFontSize(9);
		new Label (cajaAzul)
		.bindValueToProperty("precioDeVenta");
		
		new Label(cajaAzul)
		.setText("Tipo:")
		.setFontSize(9);
		new Label (cajaAzul)
		.bindValueToProperty("tipo");
		
		
		
		new Label(cajaAzul)
		.setText("Origen:")
		.setFontSize(9);
		new Label (cajaAzul)
		.bindValueToProperty("origen");
		
		
		new Label(cajaAzul)
		.setText("Precio estandar:")
		.setFontSize(9);
		new Label (cajaAzul)
		.bindValueToProperty("precioEstandar");
		
		new Label(cajaAzul)
		.setText("¿A la vanta?:")
		.setFontSize(9);
		new Label (cajaAzul)
		.bindValueToProperty("sePuedeVender");
	
		
		
		
	GroupPanel cajaNegra = new GroupPanel (arg0);
	cajaNegra.setTitle("informacion de Avion");

	
	Label cajaAmarilla = new Label (cajaNegra);
	cajaAmarilla.setText("hola");
	cajaAmarilla.setFontSize(20);
	cajaAmarilla.setBackground(Color.CYAN.darker());
	cajaAmarilla.setForeground(Color.BLUE.darker());
	cajaAmarilla.bindValueToProperty("avion.modelo");
	
					
	 Panel cajaRoja1 = new Panel(cajaNegra);/////////////////////////////////////7
		cajaRoja1.setLayout(new ColumnLayout(4));
		this.vantanaAnterior.informacionDeAvion(cajaNegra);
		
		
		Table<Pasaje> table1 = new Table<Pasaje>(arg0, Pasaje.class);
		this.setTitle("Vuelo");
		table1.bindItemsToProperty("pasajes");//retorna los vuelos (usa vuelo store)
		table1.setNumberVisibleRows(4);
	
		
		
		new Column<Pasaje>(table1) //
		    .setTitle("Nombre")
		    .setFixedSize(120)
			.bindContentsToProperty("nombre");
	
		new Column<Pasaje>(table1) //
	    .setTitle("Dni")
	    .setFixedSize(120)
		.bindContentsToProperty("dni");
		

		new Column<Pasaje>(table1) //
	    .setTitle("Fecha")
	    .setFixedSize(120)
		.bindContentsToProperty("fecha");
		
		new Column<Pasaje>(table1) //
	    .setTitle("Precio Actula")
	    .setFixedSize(120)
		.bindContentsToProperty("precio");
	
		Button detalleBu = new Button(arg0);
		detalleBu.setCaption("Agregar Pasaje");
		detalleBu.onClick(()-> {
				
			VentanaAgregarPasaje window = new VentanaAgregarPasaje(this, new Pasaje(), controlerDeVueloWindow);
			
			if(this.controlerDeVueloWindow.getComponenteSeleccionado().sePuedeVender()){
				window.open();
			}else 
				throw new RuntimeException ("No se puedo Vender Pasaje");
		
		});

	
	}
}
