package vuelo;


import java.awt.Color;
import java.time.LocalDate;

import org.uqbar.arena.bindings.ObservableProperty;
import org.uqbar.arena.widgets.Button;
import org.uqbar.arena.widgets.Label;
import org.uqbar.arena.widgets.NumericField;
import org.uqbar.arena.widgets.Panel;
import org.uqbar.arena.widgets.TextBox;
import org.uqbar.arena.windows.Dialog;


import Aerolinea.Aerolines.Pasaje;


public class VentanaAgregarPasaje extends Dialog <Pasaje> {
	protected VuelosController ventana;
	
	public LocalDate fechaActual(){
		return LocalDate.now();
	}
	
	public VentanaAgregarPasaje(InfoDeVueloWindows owner, Pasaje model,VuelosController cont) {
		super(owner, model);
		this.ventana=cont;
	}

	/**
	 * 
	 * 
	 */
	private static final long serialVersionUID = 44L;
	
	@Override
	protected void createFormPanel(Panel arg0) {
		
		new Label(arg0)
		.setText("Dni");

		new NumericField(arg0).bindValueToProperty("dni");
	
		new Label(arg0)
		.setText("NombreYApellido");
		
		new TextBox(arg0).bindValueToProperty("nombre");
		
		new Label(arg0)
		.setText("Fecha ");
		
		new Label(arg0).bindValue(new ObservableProperty<>(this,"fechaActual"));
		
		
		new Label(arg0)
		.setText("Precio a abonar ");
		new Label(arg0).bindValue(new ObservableProperty<>(ventana.getComponenteSeleccionado(),"precioDeVenta"));
		
		
	
		Button detalleBu = new Button(arg0);
		detalleBu.setCaption("Canselar");
		detalleBu.setBackground(Color.blue);
		detalleBu.setForeground(Color.darkGray);
		detalleBu.onClick(()-> { this.close();
		});
		
		Button detalle = new Button(arg0);
		detalle.setCaption("Aceptar");
		detalle.setBackground(Color.blue);
		detalle.setForeground(Color.RED.darker());
		detalle.onClick(()-> {
			this.ventana.getComponenteSeleccionado().getEmpresa().ventaDePasajes(ventana.getComponenteSeleccionado(),
					new Pasaje(this.getModelObject().getNombre(), this.getModelObject().getDni(), fechaActual()));
			this.close();
		});

	}
	
	}

	

