package vuelo;


import java.awt.Color;

import org.uqbar.arena.bindings.ObservableProperty;
import org.uqbar.arena.layout.ColumnLayout;
import org.uqbar.arena.widgets.Button;
import org.uqbar.arena.widgets.GroupPanel;
import org.uqbar.arena.widgets.Label;
import org.uqbar.arena.widgets.Panel;
import org.uqbar.arena.widgets.tables.Column;
import org.uqbar.arena.widgets.tables.Table;
import org.uqbar.arena.windows.MainWindow;

import Vuelos.Vuelo;
import Vuelos.VueloStore;


public class VueloWindow extends MainWindow<VueloStore> {
	
	private static final long serialVersionUID = 8750718241893980665L;	
	
	VuelosController vueloController = new VuelosController();
	
		public VueloWindow (VueloStore model) {
		super(model);

		}
	@Override
	public void createContents(Panel arg0) {
			this.setTitle("Vuelo");
			
			GroupPanel cajaRoja = new GroupPanel(arg0);
				cajaRoja.setTitle("VueloSeleccionado");
				cajaRoja.setLayout(new ColumnLayout(2));
		
					crearTabla(arg0);
			
			GroupPanel cajaNegra = new GroupPanel(arg0);		
				cajaNegra.setTitle("Informacìn del Aviòn");
					modeloAvion(cajaNegra);
					informacionDeAvion(cajaNegra);

		
		Button detalleBu = new Button(arg0);
		detalleBu.setCaption("abrir");
		detalleBu.onClick(()-> {
				InfoDeVueloWindows window = new InfoDeVueloWindows(this, vueloController.getComponenteSeleccionado(), vueloController);
				window.open();
		});

		}


		private void modeloAvion(GroupPanel cajaNegra) {
			Label cajaAmarilla = new Label (cajaNegra);
			cajaAmarilla.setText("hola");
			cajaAmarilla.setFontSize(20);
			cajaAmarilla.setForeground(Color.BLUE.darker());
			cajaAmarilla.setBackground(Color.CYAN.darker());
			cajaAmarilla.bindValue(new ObservableProperty<>(vueloController,  "componenteSeleccionado.avion.modelo"));
		}


		public void informacionDeAvion(GroupPanel cajaNegra) {
			Panel cajaAzul = new Panel(cajaNegra);
				cajaAzul.setLayout(new ColumnLayout(4));
				
				new Label(cajaAzul)
					.setText("Cantidad total de asientos:")
					.setFontSize(9);
				new Label (cajaAzul)
					.bindValue(new ObservableProperty<>(vueloController,"componenteSeleccionado.cantidadDeAsientos"));
				
				new Label(cajaAzul)
				.setText("Peso:")
				.setFontSize(9);
				new Label (cajaAzul)
				.bindValue(new ObservableProperty<>(vueloController,"componenteSeleccionado.avion.pesoDeAvion"));
			
				new Label(cajaAzul)
				.setText("Altura de la cabina:")
				.setFontSize(9);
				new Label (cajaAzul)
				.bindValue(new ObservableProperty<>(vueloController,"componenteSeleccionado.avion.alturaCabina"));
				
				new Label(cajaAzul)
				.setText("Consumo de nafta por kilometro:")
				.setFontSize(9);
				new Label (cajaAzul)
				.bindValue(new ObservableProperty<>(vueloController,"componenteSeleccionado.avion.consumoDeNaftaPorKilometro"));
		}


		private void crearTabla(Panel arg0) {
			Table<Vuelo> table1 = new Table<Vuelo>(arg0, Vuelo.class);
			this.setTitle("Vuelo");
			table1.bindItemsToProperty("vuelos");//retorna los vuelos (usa vuelo store)
			table1.bindValue(new ObservableProperty<>(vueloController,"componenteSeleccionado"));
			table1.setNumberVisibleRows(4);
			
		
			
			
			new Column<Vuelo>(table1) //
			    .setTitle("Nombre")
			    .setFixedSize(60)
				.bindContentsToProperty("nombre");
		
			new Column<Vuelo>(table1) //
		    .setTitle("Destino")
		    .setFixedSize(60)
			.bindContentsToProperty("destino");
			
			new Column<Vuelo>(table1) //
		    .setTitle("Origen")
		    .setFixedSize(60)
			.bindContentsToProperty("origen");
			
			new Column<Vuelo>(table1) //
		    .setTitle("Tipo")
		    .setFixedSize(60)
			.bindContentsToProperty("tipo");
			
			new Column<Vuelo>(table1) //
		    .setTitle("Asientos libres")
		    .setFixedSize(60)
			.bindContentsToProperty("cantidadDeAsientosLibres");
			
			new Column<Vuelo>(table1) //
		    .setTitle("Precio estandar")
		    .setFixedSize(60)
			.bindContentsToProperty("precioEstandar");
			
			new Column<Vuelo>(table1) //
		    .setTitle("Precio actual")
		    .setFixedSize(60)
			.bindContentsToProperty("precioDeVenta");
			
			new Column<Vuelo>(table1) //
		    .setTitle("¿A la venta?")
		    .setFixedSize(60)
			.bindContentsToProperty("sePuedeVender");
		}
}
