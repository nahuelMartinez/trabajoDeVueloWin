package vuelo;

import org.uqbar.commons.utils.Observable;

import Vuelos.Vuelo;
@Observable
public class VuelosController {
	
	private Vuelo componenteSeleccionado;
	
	public boolean hayComponenteSeleccionado (){
		return componenteSeleccionado != null;
	}
	public Vuelo getComponenteSeleccionado() {
		return componenteSeleccionado;
	}
	
	public void setComponenteSeleccionado(Vuelo selecionado) {
		componenteSeleccionado = selecionado;
	}
}
